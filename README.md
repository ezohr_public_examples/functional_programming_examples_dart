# Functional Programming Examples (Dart)

## Composition

> Composition examples

## Currying

> Currying examples

## Either

> Implementing Either using the Dartz and Freezed packages
>
> Either Unit Test using the Test Cases Framework using Left and Right results
>
> Either Unit Test using the Test Cases Framework using failedNumberValue for Left results

## Functors

> Functors examples

## Option

> Implementing Option using the Dartz package

## Recursion

> Tail and Non-tail Recursion examples (still no Tail Recursion Optimisation in Dart)
>
> Recursion examples
