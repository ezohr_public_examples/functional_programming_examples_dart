library currying_library;

// TODO: currying Unit Tests using the Test Cases Framework
// TODO: Add examples using Dartz Package

import 'package:meta/meta.dart';

Function sumLong({@required double number0}) {
  return (({@required double number1}) {
    return number0 + number1;
  });
}

Function sumShort({@required double number0}) => ({@required double number1}) => number0 + number1;

Function sumLambda = ({@required double number0}) => ({@required double number1}) => number0 + number1;
