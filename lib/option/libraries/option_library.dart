library option_library;

// TODO: option Unit Tests using the Test Cases Framework

import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

Option option({@required bool status}) {
  if (status) {
    return some(status);
  } else {
    return none();
  }
}
