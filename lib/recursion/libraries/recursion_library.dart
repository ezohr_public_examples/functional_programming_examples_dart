library recursion_library;

// TODO: recursion Unit Tests using the Test Cases Framework

import 'package:meta/meta.dart';

// Different structure to sumDownLong for Non-tail Recurision clarity
int sumDownNonTailRecursive({@required int number}) {
  if (number >= 0) {
    print('Non-tail Recursive (Running): Number: $number');
  } else {
    throw 'ERROR: Number must be >= 0';
  }

  if (number == 0) {
    return 0;
  } else {
    return number + sumDownNonTailRecursive(number: number - 1);
  }
}

// ! Still no Tail Recursion Optimisation in Dart
// Different structure to sumDownLong for Tail Recurision clarity
int sumDownTailRecursive({@required int total, @required int number}) {
  if (total >= 0 && number >= 0) {
    print('Tail Recursive (Running): Total: $total Number: $number');
  } else {
    throw 'ERROR: Total (0 recommended) and Number must be >= 0';
  }

  if (number == 0) {
    return total;
  } else {
    return sumDownTailRecursive(total: total + number, number: number - 1);
  }
}

int sumDownLong({@required int total, @required int number}) {
  if (total >= 0 && number >= 0) {
    if (number == 0) {
      return total;
    } else {
      return sumDownLong(total: total + number, number: number - 1);
    }
  } else {
    throw 'ERROR: Total (0 recommended) and Number must be >= 0';
  }
}

int sumDownShort({@required int total, @required int number}) => total >= 0 && number >= 0
    ? number == 0
        ? total
        : sumDownShort(
            total: total + number,
            // * Note the decrement order --number
            number: --number,
          )
    : throw 'ERROR: Total (0 recommended) and Number must be >= 0';

Function sumDownLambda = ({@required int total, @required int number}) => total >= 0 && number >= 0
    ? number == 0
        ? total
        : sumDownShort(
            total: total + number,
            // * Note the decrement order --number
            number: --number,
          )
    : throw 'ERROR: Total (0 recommended) and Number must be >= 0';
