library composition_library;

// TODO: composition Unit Tests using the Test Cases Framework

import 'package:built_collection/built_collection.dart';

final builtList00 = BuiltList<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
final evenNumbers = (int number) => number.isEven ? true : false;

final builtList01 = builtList00.where(evenNumbers);

final builtList10 = BuiltList<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
final squareNumbers = (int number) => number * number;

final builtList11 = builtList10.map(squareNumbers);

final builtList20 = BuiltList<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
final sumNumbers = (int total, int number) => total + number;

final builtList21 = builtList10.reduce(sumNumbers);
