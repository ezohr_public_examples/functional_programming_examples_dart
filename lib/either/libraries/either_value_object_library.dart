library either_value_object_library;

import 'package:dartz/dartz.dart';
import 'package:functional_programming_examples_dart/either/abstracts/either_value_failures_abstract.dart';
import 'package:general_libraries_examples_dart/value_object_and_common/abstracts/value_object_and_common_abstract.dart';
import 'package:meta/meta.dart';

@immutable
class RangedNumberValueObjectLibrary extends ValueObjectAbstract {
  factory RangedNumberValueObjectLibrary({@required int number}) {
    final validatedNumber = RangedNumberValueValidationLibrary.validate(number: number);

    return RangedNumberValueObjectLibrary._(value: validatedNumber);
  }

  @protected
  RangedNumberValueObjectLibrary._({@required value}) : super.private(value: value);
}

@immutable
class RangedNumberValueValidationLibrary extends ValueValidationAbstract {
  @protected
  static const _minimum = 2;
  @protected
  static const _maximum = 4;

  static Either<RangedNumberValueFailuresAbstract<int>, int> validate({@required int number}) => _minimum <= number && number <= _maximum ? right(number) : left(RangedNumberValueFailuresAbstract.invalidNumber(failedValue: number));
}
