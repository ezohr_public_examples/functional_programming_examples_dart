import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:general_libraries_examples_dart/value_object_and_common/abstracts/value_object_and_common_abstract.dart';
import 'package:meta/meta.dart';

part 'either_value_failures_abstract.freezed.dart';

// * Must run: dart pub run build_runner watch --delete-conflicting-outputs

@freezed
@immutable
abstract class RangedNumberValueFailuresAbstract<T> extends ValueFailuresAbstract with _$RangedNumberValueFailuresAbstract<T> {
  const factory RangedNumberValueFailuresAbstract.invalidNumber({@required T failedValue}) = InvalidNumber<T>;
}
