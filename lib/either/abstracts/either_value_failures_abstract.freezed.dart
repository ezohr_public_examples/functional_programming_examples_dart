// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'either_value_failures_abstract.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$RangedNumberValueFailuresAbstractTearOff {
  const _$RangedNumberValueFailuresAbstractTearOff();

// ignore: unused_element
  InvalidNumber<T> invalidNumber<T>({@required T failedValue}) {
    return InvalidNumber<T>(
      failedValue: failedValue,
    );
  }
}

// ignore: unused_element
const $RangedNumberValueFailuresAbstract =
    _$RangedNumberValueFailuresAbstractTearOff();

mixin _$RangedNumberValueFailuresAbstract<T> {
  T get failedValue;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result invalidNumber(T failedValue),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result invalidNumber(T failedValue),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result invalidNumber(InvalidNumber<T> value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result invalidNumber(InvalidNumber<T> value),
    @required Result orElse(),
  });

  $RangedNumberValueFailuresAbstractCopyWith<T,
      RangedNumberValueFailuresAbstract<T>> get copyWith;
}

abstract class $RangedNumberValueFailuresAbstractCopyWith<T, $Res> {
  factory $RangedNumberValueFailuresAbstractCopyWith(
          RangedNumberValueFailuresAbstract<T> value,
          $Res Function(RangedNumberValueFailuresAbstract<T>) then) =
      _$RangedNumberValueFailuresAbstractCopyWithImpl<T, $Res>;
  $Res call({T failedValue});
}

class _$RangedNumberValueFailuresAbstractCopyWithImpl<T, $Res>
    implements $RangedNumberValueFailuresAbstractCopyWith<T, $Res> {
  _$RangedNumberValueFailuresAbstractCopyWithImpl(this._value, this._then);

  final RangedNumberValueFailuresAbstract<T> _value;
  // ignore: unused_field
  final $Res Function(RangedNumberValueFailuresAbstract<T>) _then;

  @override
  $Res call({
    Object failedValue = freezed,
  }) {
    return _then(_value.copyWith(
      failedValue:
          failedValue == freezed ? _value.failedValue : failedValue as T,
    ));
  }
}

abstract class $InvalidNumberCopyWith<T, $Res>
    implements $RangedNumberValueFailuresAbstractCopyWith<T, $Res> {
  factory $InvalidNumberCopyWith(
          InvalidNumber<T> value, $Res Function(InvalidNumber<T>) then) =
      _$InvalidNumberCopyWithImpl<T, $Res>;
  @override
  $Res call({T failedValue});
}

class _$InvalidNumberCopyWithImpl<T, $Res>
    extends _$RangedNumberValueFailuresAbstractCopyWithImpl<T, $Res>
    implements $InvalidNumberCopyWith<T, $Res> {
  _$InvalidNumberCopyWithImpl(
      InvalidNumber<T> _value, $Res Function(InvalidNumber<T>) _then)
      : super(_value, (v) => _then(v as InvalidNumber<T>));

  @override
  InvalidNumber<T> get _value => super._value as InvalidNumber<T>;

  @override
  $Res call({
    Object failedValue = freezed,
  }) {
    return _then(InvalidNumber<T>(
      failedValue:
          failedValue == freezed ? _value.failedValue : failedValue as T,
    ));
  }
}

class _$InvalidNumber<T> implements InvalidNumber<T> {
  const _$InvalidNumber({@required this.failedValue})
      : assert(failedValue != null);

  @override
  final T failedValue;

  @override
  String toString() {
    return 'RangedNumberValueFailuresAbstract<$T>.invalidNumber(failedValue: $failedValue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is InvalidNumber<T> &&
            (identical(other.failedValue, failedValue) ||
                const DeepCollectionEquality()
                    .equals(other.failedValue, failedValue)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(failedValue);

  @override
  $InvalidNumberCopyWith<T, InvalidNumber<T>> get copyWith =>
      _$InvalidNumberCopyWithImpl<T, InvalidNumber<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result invalidNumber(T failedValue),
  }) {
    assert(invalidNumber != null);
    return invalidNumber(failedValue);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result invalidNumber(T failedValue),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (invalidNumber != null) {
      return invalidNumber(failedValue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result invalidNumber(InvalidNumber<T> value),
  }) {
    assert(invalidNumber != null);
    return invalidNumber(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result invalidNumber(InvalidNumber<T> value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (invalidNumber != null) {
      return invalidNumber(this);
    }
    return orElse();
  }
}

abstract class InvalidNumber<T>
    implements RangedNumberValueFailuresAbstract<T> {
  const factory InvalidNumber({@required T failedValue}) = _$InvalidNumber<T>;

  @override
  T get failedValue;
  @override
  $InvalidNumberCopyWith<T, InvalidNumber<T>> get copyWith;
}
