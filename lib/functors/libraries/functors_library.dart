library functors_library;

// TODO: functors Unit Tests using the Test Cases Framework

import 'package:built_collection/built_collection.dart';

// BuiltList is a Functor since it has a map method
final builtList = BuiltList<int>([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
final zeroElement = <T>({T element}) => (T value) => value != element ? value : 0;

final builtList0 = builtList.map(zeroElement(element: 5));
