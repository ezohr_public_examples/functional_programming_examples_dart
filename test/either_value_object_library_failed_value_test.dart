import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:functional_programming_examples_dart/either/abstracts/either_value_failures_abstract.dart';
import 'package:functional_programming_examples_dart/either/libraries/either_value_object_library.dart';
import 'package:general_libraries_examples_dart/test_cases/abstracts/test_cases_abstract.dart';
import 'package:test/test.dart';

@immutable
class TestCaseStructure extends TestCaseStructureAbstract {
  final int number;

  TestCaseStructure({@required positive, @required description, @required matcher, @required this.number}) : super(positive: positive, description: description, matcher: matcher);
}

@immutable
class TestCases extends TestCasesAbstract {
  @protected
  @override
  final String testGroup = 'Either Value Object Library';

  @override
  @protected
  List<TestCaseStructure> definedTestCases() {
    // TODO: Can this be made immutable
    var testCases = <TestCaseStructure>[];

    testCases.add(TestCaseStructure(
      positive: true,
      description: '2',
      matcher: 2,
      number: 2,
    ));

    testCases.add(TestCaseStructure(
      positive: true,
      description: '3',
      matcher: 3,
      number: 3,
    ));

    testCases.add(TestCaseStructure(
      positive: true,
      description: '4',
      matcher: 4,
      number: 4,
    ));

    testCases.add(TestCaseStructure(
      positive: false,
      description: '-1',
      matcher: -1,
      number: -1,
    ));

    testCases.add(TestCaseStructure(
      positive: false,
      description: '0',
      matcher: 0,
      number: 0,
    ));

    testCases.add(TestCaseStructure(
      positive: false,
      description: '1',
      matcher: 1,
      number: 1,
    ));

    testCases.add(TestCaseStructure(
      positive: false,
      description: '5',
      matcher: 5,
      number: 5,
    ));

    return testCases;
  }
}

@immutable
class ClassTests extends ClassTestsAbstract {
  ClassTests({@required String testGroup, @required List<TestCaseStructure> testCases}) : super(testGroup: testGroup, testCases: testCases);

  @override
  @protected
  void testTestCase({@required TestCaseStructureAbstract testCase}) {
    final testCaseDowncast = testCase as TestCaseStructure;

    final testType = testCaseDowncast.positive ? 'Positive:' : 'Negative:';

    test('$testType ${testCaseDowncast.description}', () async {
      // Arrange
      final testClass = RangedNumberValueObjectLibrary(number: testCaseDowncast.number);

      // Act
      final actual = testClass.getValue.fold(
        (l) => (l as RangedNumberValueFailuresAbstract).failedValue,
        (r) => r,
      );

      // Assert
      expect(actual, testCaseDowncast.matcher);
    });
  }
}

void main() {
  final testCases = TestCases();

  ClassTests(testGroup: testCases.getTestGroup, testCases: testCases.getTestCases);
}
